# Documents pour la classe de TS2 du lycée Colbert.

## Rappels trigonométrie
* [Cours](8_fonctions_trigonometriques/cours-trigo.pdf)
* [Exercices](8_fonctions_trigonometriques/ex-trigo.pdf)
* [Exercice tangente](8_fonctions_trigonometriques/ex-tangente.pdf) Une correction [ici](doc.beechannels.com/baccalaureat/DM%20-%20Etude%20de%20la%20fonction%20tangente.pdf)

## Documents sur l'intégration
* [Cours intégration](10_integration/cours-integration.pdf)
* [Exercices intégration](10_integration/ex-integration.pdf)
* [Activité lien aire-primitive](10_integration/act-integration.pdf)

## Nombres complexes II
* [Cours](9_nombres_complexes_2/cours-nombresComplexes2.pdf)
* [Exercice nombres complexes](9_nombres_complexes_2/ex-nb_complexes2.pdf)
* [Exercice lien géométrie et nombres complexes](9_nombres_complexes_2/ex_nb_complexes_g%C3%A9om%C3%A9trie.pdf)
* [Exercice type bac avec des nombres complexes](9_nombres_complexes_2/ex-nb_complexes_bac.pdf)
* [Fiche utilisation de la calculatrice](http://math.univ-lyon1.fr/irem/IMG/pdf/501_kit_TS_Graph35_E_2015.pdf) Le bas de la page 2 est consacrée aux nombres complexes.


## Géométrie dans l'espace
* [Cours](11_geometrie_espace/cours_geometrie_espace.pdf)
* [exercices introduction](11_geometrie_espace/fiche-positionRelative.pdf)
* [exercices type bac](11_geometrie_espace/fiche-positionRelative.pdf)
* Pour arriver à mieux voir, ayez avec vous un parallélépipède rectangle (boite d'allumettes, boite à chaussures, etc). Vous pouvez aussi vous en construire un.
Vous trouver [ici](https://www.polyhedra.net/pdf/rectangular_prism.pdf) un patron.

## Lois à densité
* [Cours](12_lois_densit%C3%A9/cours-loisDensite.pdf)
* [Loi normale et calculatrice](http://mathematiques.ac-bordeaux.fr/lycee2010/calculatrices/loi_normale_et_calculatrice.pdf)
* [Exercice bac loi exponentiellle](12_lois_densit%C3%A9/ex-bac.pdf)
* [Exercices bac loi normale](12_lois_densit%C3%A9/ex-bac-loi_normale.pdf)

## Estimation
* [cours](13_estimation/cous-estimation.pdf)
* [Exercices  bac estimation](13_estimation/ex-bac-estimation.pdf)
# Trace écrite de la classe virtuelle
Vous trouverez dans la mesure du possible ce que j'ai écrit organisé par date [ici](traceEcrite). La date est de la forme mmjj ainsi le fichier du 1 avril s'appelle ```0401.pdf```

# Objectif l'enseignement supérieur

Vous pouvez travailler tous les exercices sur les suites numériques, les fonctions hormis les questions sur l'intégration, les probabilités conditionnelles.
Vous trouverez sur le site de l'APMEP tous les sujets et leurs corrigés. 
* [Sujets de l'anée 2018](https://www.apmep.fr/S-Annee-2018-12-sujets-12-corriges)
* [L'ensemble des sujets avec un index pas notion à la fin](https://www.apmep.fr/IMG/pdf/S_ANNEE_2018_2.pdf)
* [Très bonne synthèse du programme  de mathématiques obligatoires par Paul Milan](https://www.lyceedadultes.fr/sitepedagogique/documents/math/mathTermS/00_Referentiel/tout_ce_qu_il_faut_savoir_termS.pdf)
* Le [DST n°5](dst/dst5.pdf) et son [corrigé](dst/dst5-corr.pdf)